def read_file():
    with open("input.tex", "r") as f:
        text = f.read()
        f.close()
        return text


def write_to_file(text: str):
    with open("output.tex", "w+") as f:
        f.write(text)
        f.close()


def change_content(text: str) -> str:
    new_text = text
    quote_found = True
    quote_position = -1
    quoted = []
    while quote_found:
        quote_position = new_text.find('"', quote_position + 1) + 1
        if quote_position == 0:
            quote_found = False
            continue
        q1 = quote_position
        new_text = new_text[:quote_position - 1] + r"\foreignquote{german}{" + new_text[quote_position:]

        quote_position = new_text.find('"', quote_position + 1) + 1
        q2 = quote_position
        new_text = new_text[:quote_position - 1] + "}" + new_text[quote_position:]

        quoted.append(new_text[q1+21:q2-1])
    print("The following quotations have been identified: " + str(quoted))
    return new_text


new_changes = change_content(read_file())
write_to_file(new_changes)
print("The input has been Analysed. Check the new output.tex file.")
print(r"Note: Remember to put the \usepackage{csquotes} into your file, so that latex installs the package.")
print("Press enter to continue...")
input()
